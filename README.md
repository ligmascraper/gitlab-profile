# Ligma Scraper
## Overview
A tool, that used to be on my website and called ligma scraper. It connects to https://www.sigmaaldrich.com allowing you to search for a chemical and pull safety- and other relevant information off their site, in order to make labelling chemicals in the hobby lab less tedious.

### Online webassembly implementation
https://ligmascraper.gitlab.io/wasm/

### Library Documentation
https://pkg.go.dev/gitlab.com/ligmascraper/ligma

### Examples
See https://gitlab.com/ligmascraper/ligmascraper-cli/ for a use case

## See also
https://pubchem.ncbi.nlm.nih.gov/ghs/

## Authors
Conceived and built by Holger M Mürk
